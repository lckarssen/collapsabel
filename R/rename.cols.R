# rename selected columns of a data frame by a given function
rename.cols(x, cols, fun) %::% data.frame : integer : Function : data.frame
rename.cols(x, cols, fun) %when% {
    max(cols) <= ncol(x)
    min(cols) >= 1
    !is.null(colnames(x))
} %as% {
    colnames(x)[cols] = fun(colnames(x)[cols])
    x
}

sort.cols(x, ...) %::% data.frame : ... : data.frame
sort.cols(x, ...) %when% {
    !is.null(colnames(x))
} %as% {
    x[, sort(colnames(x), ...)]
}


#' gCDH routine
#'
#' This routine generates arguments to be passed to PLINK and task name automatically,
#' then pass these to routine 1.
#'
#' @param wDir Working directory. All paths within this function will be relative to this.
#' @param pheno Phenotype file (usually also containing covariates)
#' @param pheno_name Name of phenotype in the phenotype file
#' @param covar Covariates file, default to the same as \code{pheno}
#' @param covar_name Name of covariates in the covariates file
#' @param plinkArgs Arguments to be passed to PLINK (in gCDH analysis)
#' @param initGwas Whether to perform an initial GWAS
#' @param initGwasArgs Arguments to pass to PLINK for the initial GWAS
#' @param pFilter Filter out SNPs with p value higher than this (in the initial GWAS)
#' @param nMaxShift Maximum shift number (for genotype collapsing)
#' @param bpdiff Upper bound of distance between a pair of SNPs, default to 5e5
#' @param rmbed Logical. Whether to remove shifted bed files after analysis
#' @return hubtask An environment containing info and results from the current task.
#' @export
gcdhRoutine = function(wDir=".", pheno, pheno_name, covar=NULL, covar_name=NULL, plinkArgs, initGwas=FALSE, initGwasArgs, pFilter, nMaxShift, bpdiff = 5e5, rmbed=TRUE) {
    checkFileExists(c(wDir, pheno))

    plinkArgs = within(plinkArgs, {
        pheno = pheno
        pheno_name = pheno_name
    })
    if(!is.null(covar) && !is.null(covar_name)) {
        checkFileExists(covar)
        plinkArgs = within(plinkArgs, {
            covar = covar
            covar_name = covar_name
        })
    }
    else if(is.null(covar) && !is.null(covar_name)) {
        plinkArgs$covar = plinkArgs$pheno
        plinkArgs$covar_name = covar_name
    }
    else if(!is.null(covar) && is.null(covar_name)) {
        checkFileExists(covar)
        plinkArgs$covar = covar
        plinkArgs$covar_name = readline("You provided a covar file, but forgot to provide covar names, please input here: ")
    }


    initGwasArgs = within(initGwasArgs, {
        pheno = pheno
        pheno_name = pheno_name
        assoc = ""
    })

    if(!is.null(covar_name)) {
        taskName = sprintf("%s-%s_s%d_p%.2e", pheno_name, plinkArgs$covar_name, nMaxShift, pFilter)
    } else {
        taskName = sprintf("%s_s%d_p%.2e", pheno_name, nMaxShift, pFilter)
    }

    hubtask = routine1(
        wDir = wDir,
        taskName = taskName,
        plinkArgs = plinkArgs,
        initGwasArgs = initGwasArgs,
        pFilter = pFilter,
        initGwas = initGwas,
        nMaxShift = nMaxShift,
        rmbed = rmbed
    )
    invisible(hubtask)
}


# generate random genotypes, assuming MAF 0.1 by default
rand.genodat(n, k, p = c(0.81, 0.18, 0.01)) %::% integer : integer : . : GenoMatrix
rand.genodat(n, k, p = c(0.81, 0.18, 0.01)) %as% {
    p = ProbVec(p)
    rand.geno = function() {
        sample(0:2, n, repl = TRUE, prob = p)
    }
    ret = sapply(1:k, function(i) rand.geno()) %>% as.data.frame
    colnames(ret) = paste("g", 1:k, sep="")
    GenoMatrix(ret, 1:k)
}

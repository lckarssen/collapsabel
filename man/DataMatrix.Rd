% Generated by roxygen2 (4.1.0): do not edit by hand
% Please edit documentation in R/DataMatrix.R
\name{DataMatrix}
\alias{DataMatrix}
\title{A type wrapper around matrix or data.frame}
\arguments{
\item{x}{The data.frame or matrix to wrap over.}
}
\value{
A DataMatrix object.
}
\description{
This is useful for a function that receives either a matrix or a data.frame as args.
}

